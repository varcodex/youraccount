import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { Collector } from '../../model/collector/collector';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Reference } from '@angular/fire/storage/interfaces';
import * as firebase from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class CollectorService {
  formData: Collector;
  constructor(private http: HttpClient,
              private fireStorage: AngularFireStorage,
              private fireStore: AngularFirestore) { }

  getCollector(uid): Observable<Collector[]> {
    return this.http.get<Collector[]>(`${environment.yaFunctions.url}/collector/${uid}`);
  }

  getRole(uid): Observable<Collector[]> {
    return this.http.get<Collector[]>(`${environment.yaFunctions.url}/getrole/${uid}`);
  }

  getCollectors(): Observable<Collector[]> {
    return this.http.get<Collector[]>(`${environment.yaFunctions.url}/collector`);
  }

  async updateCollectorsClient(collectorId, clientId){
    const  arrayUnion = firebase.firestore.FieldValue.arrayUnion;
    const documentRef = await this.fireStore.doc(`collector/${collectorId}`);
    await documentRef.update(
        {clientId: arrayUnion(clientId)}
    );
    return true;
  }

  async createCollector(collector: Partial<Collector>){
    return await this.fireStore.collection('collector').add(collector);
  }

  getCollectorAvatar(id: string): Promise<Reference> {
    return new Promise(async resolve => {
      try {
        const clientFiles = this.fireStorage.storage.ref(`/collector/${id}/avatar`);
        const listResult = await clientFiles.list({maxResults: 1});
        resolve(listResult.items[0]);
      } catch (error) {
        console.log(error);
        resolve(null);
      }
    });
  }

  getCollectorByID(id: string) {
    const collectorData = this.fireStore.doc('collector/' + id);
    return collectorData;
  }

  async deleteCollector(id: string) {
    return await this.fireStore.doc('collector/' + id).delete();
  }

  async updateCollector(id, collector: Partial<Collector>) {
    await this.fireStore.doc(`collector/${id}`).set(collector, {merge: true});
  }

  async updateWithUploadAvataCollector(id, collector: Partial<Collector>) {
    await this.fireStore.doc(`collector/${id}`).set(collector, {merge: true});
  }
}
