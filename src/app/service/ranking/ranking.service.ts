import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { Ranking } from '../../model/ranking/ranking';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RankingService {
  formData: Ranking;
  constructor(private http: HttpClient,
    private fireStorage: AngularFireStorage,
    private fireStore: AngularFirestore) { }

    startCollection(uid): Observable<Ranking[]> {
      return this.http.get<Ranking[]>(`${environment.yaFunctions.url}/ranking/${uid}`);
    }

    async updateRanking(collectorId, clientId){
      await this.fireStore.doc(`collector/${collectorId}`).set({
        clientId: clientId},
        {merge: true}
      );
      return true;
  }
}
