import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { Lista } from '../../model/client/lista';
import { Clients } from '../../model/client/clients';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ListaService {
  formData: Lista;
  constructor(private http: HttpClient,
    private fireStorage: AngularFireStorage,
    private fireStore: AngularFirestore) { }
  
  getLista(id: string): Observable<Lista[]> {
    return this.http.get<Lista[]>(`${environment.yaFunctions.url}/mylista/${id}`);
  }

  getListaByID(listaId: string) {
    const listaData = this.fireStore.doc(`lista/${listaId}`);
    return listaData;
  }

  async createLista(lista: Partial<Lista>){
    return await this.fireStore.collection('lista').add(lista);
  }

  async updateLista(listaId: string, lista: Partial<Lista>) {
    return await this.fireStore.doc(`/lista/${listaId}`).set(lista, {merge: true});
  }

  async updateClientListaNo(id: string, client: Partial<Clients>) {
    return await this.fireStore.doc(`client/${id}`).set(client, {merge: true});
  }

  async deleteLista(listaId: string) {
    return await this.fireStore.doc(`/lista/${listaId}`).delete();
  }
}
