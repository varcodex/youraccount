import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { Collection } from '../../model/client/collection';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CollectionService {
  formData: Collection;
  constructor(private http: HttpClient,
    private fireStorage: AngularFireStorage,
    private fireStore: AngularFirestore) { }

  getCollection(id: string): Observable<Collection[]> {
    return this.http.get<Collection[]>(`${environment.yaFunctions.url}/mycollection/${id}`);
  }

  async createCollection(collection: Partial<Collection>){
    return await this.fireStore.collection('collection').add(collection);
  }

  async updateLista(listaId: string, amountPaid: string, balance: string, status: string) {
    return await this.fireStore.doc(`/lista/${listaId}`)
    .update({amountPaid: amountPaid,
            balance: balance,
            status: status,});
  }

  async deleteCollection(id: string) {
    return await this.fireStore.doc(`collection/${id}`).delete();
  }
}
