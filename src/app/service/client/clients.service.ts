import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { Clients } from '../../model/client/clients';
import { Notification } from '../../model/notification/notification';
import { Dashboard } from '../../model/dashboard/dashboard';
import { ClientNo } from '../../model/client/client-no';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Reference } from '@angular/fire/storage/interfaces';
import { environment } from '../../../environments/environment';
import * as firebase from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class ClientsService {
  formData: Clients;
  constructor(private http: HttpClient,
              private fireStorage: AngularFireStorage,
              private fireStore: AngularFirestore) { }

  getClients(uid): Observable<Clients[]> {
    return this.http.get<Clients[]>(`${environment.yaFunctions.url}/clients/${uid}`);
  }
  
  getAssignClient(uid): Observable<Clients[]> {
    return this.http.get<Clients[]>(`${environment.yaFunctions.url}/clientAssign/${uid}`);
  }
  getNotification(): Observable<Notification[]> {
    return this.http.get<Notification[]>(`${environment.yaFunctions.url}/notification`);
  }

  getClientonTrack(uid): Observable<Clients[]> {
    return this.http.get<Clients[]>(`${environment.yaFunctions.url}/clientOntrack/${uid}`);
  }

  getClientOverdue(uid): Observable<Clients[]> {
    return this.http.get<Clients[]>(`${environment.yaFunctions.url}/clientOverdue/${uid}`);
  }

  getDashboard(uid): Observable<Dashboard[]> {
    return this.http.get<Dashboard[]>(`${environment.yaFunctions.url}/dashboard/${uid}`);
  }

  getClientNo(): Observable<ClientNo[]> {
    return this.http.get<ClientNo[]>(`${environment.yaFunctions.url}/ClientNo`);
  }

  getClient(id: string): Observable<Clients[]> {
    return this.http.get<Clients[]>(`${environment.yaFunctions.url}/client/${id}`);
  }

  getClientByID(id: string) {
    const clientData = this.fireStore.doc('client/' + id);
    return clientData;
  }

  getClientAvatar(id: string): Promise<Reference> {
    return new Promise(async resolve => {
      try {
        const clientFiles = this.fireStorage.storage.ref(`/client/${id}/avatar`);
        const listResult = await clientFiles.list({maxResults: 1});
        resolve(listResult.items[0]);
      } catch (error) {
        console.log(error);
        resolve(null);
      }
    });
  }

  async createClient(client: Partial<Clients>, collectorId){
     return await this.fireStore.collection('client').add(client).then(async docRef => {
      const  arrayUnion = firebase.firestore.FieldValue.arrayUnion;
      const documentRef = await this.fireStore.doc(`collector/${collectorId}`);
      await documentRef.update(
          {clientId: arrayUnion(docRef.id)}
        );
    });
  }

  async updateClient(userId: string, client: Partial<Clients>) {
        return await this.fireStore.doc(`client/${userId}`).set(client, {merge: true});
  }

  async updateClientNo(id: string, clientNo: Partial<ClientNo>) {
    return await this.fireStore.doc(`clientNo/${id}`).set(clientNo, {merge: true});
  }

  async deleteClient(id: string) {
    return await this.fireStore.doc('client/' + id).delete();
  }

  async updateListaStatus(listaId: string, status: string) {
    return await this.fireStore.doc(`/lista/${listaId}`)
    .update({status});
  }

  async updateClientStatus(clientId: string, status: string) {
    return await this.fireStore.doc(`/client/${clientId}`)
    .update({status});
  }

}
