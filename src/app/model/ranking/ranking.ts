export interface Ranking {
    collectorId: string;
    clientId: string;
    rank: number;
    firstName: string;
    lastName: string;
    clientAvatarUrl: string;
}
