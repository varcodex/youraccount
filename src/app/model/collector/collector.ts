export interface Collector {
    id: string;
    collectorId: string;
    uuid: string;
    clientId: string[];
    firstName: string;
    lastName: string;
    address: string;
    gender: string;
    phoneNumber: string;
    status: string;
    role: string;
    collectorAvatarUrl: string;
    createdAt: string;
    updatedAt: string;
}
