export interface Clients {
    id: string;
    clientId: string;
    collectorId: string;
    lista: string;
    firstName: string;
    lastName: string;
    address: string;
    gender: string;
    phoneNumber: string;
    status: string;
    is_assigned: boolean;
    clientAvatarUrl: string;
    createdAt: string;
    updatedAt: string;
}