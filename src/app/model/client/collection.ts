export interface Collection {
    id: string;
    clientId: String;
    dpa: string;
    balance: string;
    firstName: string;
    lastName: string;
    alias: string;
    amount: string;
    amountPaid: string;
    listaId: string;
    clientAvatarUrl: string;
    createdAt: string;
    updatedAt: string;
}
