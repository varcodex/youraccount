export interface Lista {
    id: string;
    listaId: string;
    clientId: string;
    description: string;
    alias: string;
    status: string;
    startDate: string;
    endDate: string;
    dpa: string;
    totalAmount: string;
    amountPaid: string;
    balance: string;
    createdAt: string;
    updatedAt: string;
    color: string;
}
