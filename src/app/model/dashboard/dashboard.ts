export interface Dashboard {
    totalClient: string;
    onTrackClient: string;
    onOverdueClient: string;
}
