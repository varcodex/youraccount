export interface Notification {
    id: string;
    clientId: string;
    lista: string;
    firstName: string;
    lastName: string;
    address: string;
    gender: string;
    phoneNumber: string;
    clientStatus: string;
    listaStatus: string;
    clientAvatarUrl: string;
    myListaSnapId: string,
    listaId: string;
    description: string;
    alias: string;
    startDate: string;
    endDate: string;
    dpa: string;
    totalAmount: string;
    amountPaid: string;
    balance: string;
    createdAt: string;
    updatedAt: string;
    color: string;
}
