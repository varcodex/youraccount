import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenuPage } from './menu.page';

const routes: Routes = [
  {
    path: '',
    component: MenuPage,
    children: [
      {
        path: 'client',
        loadChildren: () => import('../client/client.module').then( m => m.ClientPageModule)
      },
      {
        path: 'client/new-client',
        loadChildren: () => import('../client/new-client/new-client.module').then( m => m.NewClientPageModule)
      },
      {
        path: 'collection',
        loadChildren: () => import('../collection/collection.module').then( m => m.CollectionPageModule)
      },
      {
        path: 'ranking',
        loadChildren: () => import('../ranking/ranking.module').then( m => m.RankingPageModule)
      },
      {
        path: 'collector',
        loadChildren: () => import('../collector/collector.module').then( m => m.CollectorPageModule)
      },
      {
        path: 'dashboard',
        loadChildren: () => import('../dashboard/dashboard.module').then( m => m.DashboardPageModule)
      },
      {
        path: 'notification',
        loadChildren: () => import('../notification/notification.module').then( m => m.NotificationPageModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuPageRoutingModule {}
