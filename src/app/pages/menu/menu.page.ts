import { Component, OnInit } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ClientsService } from "../../service/client/clients.service";
import { Notification } from '../../model/notification/notification';
@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {
  selectedPath = '';
  pages:any;
  notification:any;
  clients: Notification[] = [];
  constructor(private router: Router,
    public clientsService: ClientsService,
    private storage: Storage) { 
    this.router.events.subscribe((event: RouterEvent) => {
        if(event && event.url){
          this.selectedPath = event.url;
        }
    })
  }

   ngOnInit() {
    this.storage.get('notification').then((val) => {
      this.notification = val;
    });
    this.storage.get('role').then(async (val) => {
      if(val == 'administrator'){
        this.clients = await this.clientsService.getNotification().toPromise();
        this.notification = this.clients.length;
        this.pages =[ 
          {
            title: 'Dashboard',
            url: '/menu/dashboard'
          },
          {
            title: 'Start Collection',
            url: '/menu/collection'
          },
          {
            title: 'Client',
            url: '/menu/client'
          },
          {
            title: 'Collector',
            url: '/menu/collector'
          },
          {
            title: 'Notification',
            url: '/menu/notification'
          },
          {
            title: 'Ranking',
            url: '/menu/ranking'
          }
        ];
      }else if(val == 'collector'){
        this.pages =[ 
          {
            title: 'Dashboard',
            url: '/menu/dashboard'
          },
          {
            title: 'Start Collection',
            url: '/menu/collection'
          },
          {
            title: 'Client',
            url: '/menu/client'
          },
          {
            title: 'Ranking',
            url: '/menu/ranking'
          }
        ];
      }
    });
  }

  notificationCount(title) {
    switch (title) {
        case 'Notification': return this.notification; 
    }
  }

}
