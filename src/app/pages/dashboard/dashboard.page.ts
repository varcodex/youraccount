import { Component, OnInit } from '@angular/core';
import { ClientsService } from '../../service/client/clients.service';
import { Dashboard } from '../../model/dashboard/dashboard';
import { Router } from "@angular/router";
import { Storage } from '@ionic/storage';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {
  clients: Dashboard[] = [];
  uid: any;
  constructor(public clientsService: ClientsService,
    private router: Router,
    private storage: Storage) { }

  async ngOnInit() {
    await this.storage.get('uid').then((val) => {
      this.uid = val;
    });
    this.clients = await this.clientsService.getDashboard(this.uid).toPromise();
  }

}
