import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { ClientsService } from '../../../../service/client/clients.service';
import { ListaService } from '../../../../service/client/lista.service';
import { Lista } from '../../../../model/client/lista';
import { Clients } from '../../../../model/client/clients';
import { Storage } from '@ionic/storage';
@Component({
  selector: 'app-lista',
  templateUrl: './lista.page.html',
  styleUrls: ['./lista.page.scss'],
})
export class ListaPage implements OnInit {
  id: any;
  listas: Lista[] = [];
  filteredLista: Lista[] = [];
  clients: Clients[] = [];
  clientId: any;
  lista: any;
  clientName: any;
  uid: any;

  constructor(public listaService: ListaService,
    public clientsService: ClientsService,
    private router: Router,
    private actRoute: ActivatedRoute,
    private storage: Storage) {
      this.id = this.actRoute.snapshot.paramMap.get('id');
    }

  async ngOnInit() {
    this.listas = await this.listaService.getLista(this.id).toPromise();
    this.clients = await this.clientsService.getClient(this.id).toPromise();
    for (const client of this.clients) {
      if (client.id) {
          this.clientId = client.clientId;
          this.lista = client.lista;
          this.clientName = client.firstName +' '+ client.lastName;
          const clientAvatar = await this.clientsService.getClientAvatar(client.id);
          if (clientAvatar) {
            client.clientAvatarUrl = await clientAvatar.getDownloadURL();
          }else{
            client.clientAvatarUrl = `assets/imgs/blank-avatar.jpg`;
          }
      }
    }
    for (const lista of this.listas) {
      if(lista){
        if(lista.status == 'Completed'){
          lista.color = 'success'
        }else if(lista.status == 'On Track'){
          lista.color = 'primary'
        }else if(lista.status == 'Overdue'){
          lista.color = 'danger'
        }
      }
    }
  }

  mycollection(id, listaid, startDate, endDate, totalAmount, dpa, amountPaid, balance, status) {
      this.storage.set('startDate', startDate.toString());
      this.storage.set('endDate', endDate.toString());
      this.storage.set('totalAmount', totalAmount.toString());
      this.storage.set('dpa', dpa.toString());
      this.storage.set('amountPaid', amountPaid.toString());
      this.storage.set('balance', balance.toString());
      this.storage.set('status', status.toString());
      this.storage.set('clientName', this.clientName.toString());
      this.router.navigate(['/menu/client/lista/'+id+'/collection/'+listaid]);
  }

  async doRefresh(event) {  
    setTimeout(async () => {
      this.listas = await this.listaService.getLista(this.id).toPromise();
      this.listas = await this.listaService.getLista(this.id).toPromise();
      for (const lista of this.listas) {
        if(lista){
          if(lista.status == 'Completed'){
            lista.color = 'success'
          }else if(lista.status == 'On Track'){
            lista.color = 'primary'
          }else if(lista.status == 'Overdue'){
            lista.color = 'danger'
          }
        }
      }
      event.target.complete();
      }, 1500); 
  }

}
