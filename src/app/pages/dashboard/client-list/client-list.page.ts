import { Component, OnInit } from '@angular/core';
import { ClientsService } from '../../../service/client/clients.service';
import { Clients } from '../../../model/client/clients';
import { ActivatedRoute, Router } from '@angular/router';
import { Storage } from '@ionic/storage';
@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.page.html',
  styleUrls: ['./client-list.page.scss'],
})
export class ClientListPage implements OnInit {
  clients: Clients[] = [];
  filteredClients: Clients[] = [];
  uid: any;
  status: any;
  constructor(public clientsService: ClientsService,
              private router: Router,
              private actRoute: ActivatedRoute,
              private storage: Storage) {
      this.status = this.actRoute.snapshot.paramMap.get('status');
    }

  async ngOnInit() {
    await this.storage.get('uid').then((val) => {
      this.uid = val;
    });
    if (this.status === 'On Track'){
      this.clients = await this.clientsService.getClientonTrack(this.uid).toPromise();
    }else if (this.status === 'Overdue'){
      this.clients = await this.clientsService.getClientOverdue(this.uid).toPromise();
    }
  }

  async doRefresh(event) {
    setTimeout(async () => {
      if (this.status === 'On Track'){
        this.clients = await this.clientsService.getClientonTrack(this.uid).toPromise();
      }else if (this.status === 'Overdue'){
        this.clients = await this.clientsService.getClientOverdue(this.uid).toPromise();
      }
      for (const client of this.clients) {
        if (client.id) {
            const clientAvatar = await this.clientsService.getClientAvatar(client.id);
            if (clientAvatar) {
              client.clientAvatarUrl = await clientAvatar.getDownloadURL();
            }else{
              client.clientAvatarUrl = `assets/imgs/blank-avatar.jpg`;
            }
        }
      }
      event.target.complete();
      }, 1500);
  }

}
