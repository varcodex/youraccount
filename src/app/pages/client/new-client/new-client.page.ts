import { Component, OnInit } from '@angular/core';
import { ClientsService } from '../../../service/client/clients.service';
import { Clients } from '../../../model/client/clients';
import { ClientNo } from '../../../model/client/client-no';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { formatDate } from '@angular/common';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { CollectorService } from '../../../service/collector/collector.service';

@Component({
  selector: 'app-new-client',
  templateUrl: './new-client.page.html',
  styleUrls: ['./new-client.page.scss'],
})

export class NewClientPage implements OnInit {
  newClientForm: FormGroup;
  clients: Clients;
  clientNo: ClientNo[] = [];
  clientId: any;
  collectorId: any;
  clientNoDoc: any;
  options: any;
  uid: any;


  constructor(public clientsService: ClientsService,
              public collectorService: CollectorService,
              private formBuilder: FormBuilder,
              private router: Router,
              private storage: Storage) {
                this.newClientForm = this.formBuilder.group({
                  firstName: ['', Validators.required],
                  lastName : [''],
                  address: ['', Validators.required],
                  phoneNumber: [''],
                  gender: [Validators.required]
                }, {
                  updateOn: 'change'
                });
              }

  async ngOnInit() {
    await this.storage.get('uid').then((val) => {
      this.uid = val;
    });
    await this.storage.get('collectorId').then((val) => {
      this.collectorId = val;
    });
    this.clientNo = await this.clientsService.getClientNo().toPromise();
    for (const c of this.clientNo) {
      if (c.clientNo) {
         this.clientId = c.clientNo;
         this.clientNoDoc = c.id;
      }
    }
  }

  async createclient() {
    const convertClientId = Number(this.clientId);
    const clientid = convertClientId + 1;
    await this.clientsService.createClient(
        {
          clientId: clientid.toString(),
          lista: '0',
          firstName: this.firstName.value,
          lastName: this.lastName.value,
          address: this.address.value,
          phoneNumber: this.phoneNumber.value,
          gender: this.gender.value,
          status: 'Inactive',
          collectorId: this.uid,
          createdAt: formatDate(new Date(), 'yyyy-MM-dd\'T\'HH:mm:ss', 'en-PH', '+08:00'),
          updatedAt: formatDate(new Date(), 'yyyy-MM-dd\'T\'HH:mm:ss', 'en-PH', '+08:00'),
        },
        this.collectorId
      ).then(async () => {
        this.router.navigate(['menu/client']);
        window.alert('New Client Successfully Created!');
      })
      .catch(error => console.log(error));

    await this.clientsService.updateClientNo(this.clientNoDoc, {
          clientNo: clientid.toString(),
      });
  }

  get firstName() {
    return this.newClientForm.get('firstName');
  }

  get lastName() {
    return this.newClientForm.get('lastName');
  }

  get address() {
    return this.newClientForm.get('address');
  }

  get phoneNumber() {
    return this.newClientForm.get('phoneNumber');
  }

  get gender() {
    return this.newClientForm.get('gender');
  }
}
