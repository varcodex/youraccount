import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListaPage } from './lista.page';

const routes: Routes = [
  {
    path: '',
    component: ListaPage
  },
  {
    path: 'new-lista/:clientId/:lista',
    loadChildren: () => import('./new-lista/new-lista.module').then( m => m.NewListaPageModule)
  },
  {
    path: 'edit-lista/:listaId',
    loadChildren: () => import('./edit-lista/edit-lista.module').then( m => m.EditListaPageModule)
  },
  {
    path: 'collection/:listaId',
    loadChildren: () => import('./collection/collection.module').then( m => m.CollectionPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListaPageRoutingModule {}
