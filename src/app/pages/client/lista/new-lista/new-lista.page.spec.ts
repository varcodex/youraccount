import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NewListaPage } from './new-lista.page';

describe('NewListaPage', () => {
  let component: NewListaPage;
  let fixture: ComponentFixture<NewListaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewListaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NewListaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
