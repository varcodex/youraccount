import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { formatDate } from '@angular/common';
import { Lista } from '../../../../model/client/lista';
import { ListaService } from '../../../../service/client/lista.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-new-lista',
  templateUrl: './new-lista.page.html',
  styleUrls: ['./new-lista.page.scss'],
})
export class NewListaPage implements OnInit {
  id: any;
  newListaForm: FormGroup;
  lista: Lista;
  clientId: any;
  listaNo: any;
  constructor(public listaService: ListaService,
    private router: Router,
    private storage: Storage,
    private formBuilder: FormBuilder,
    private actRoute: ActivatedRoute) {
      this.id = this.actRoute.snapshot.paramMap.get('id');
      this.clientId = this.actRoute.snapshot.paramMap.get('clientId');
      this.listaNo = this.actRoute.snapshot.paramMap.get('lista');
    }

  ngOnInit() {
    this.newListaForm = this.formBuilder.group({
      description: ['', Validators.required],
      alias : [''],
      totalAmount: ['', Validators.required],
      startDate : ['', Validators.required],
      endDate : ['', Validators.required],
      dpa: ['', Validators.required],
    }, {
      updateOn: 'change'
    });
  }

  async createlista(){
    var convertListaNo = Number(this.listaNo);
    const listaNo = convertListaNo + 1;
    const listaId = ("000"+this.clientId).slice(-4) +'-'+ listaNo.toString();
    this.storage.get('role').then(async (val) => {
      let status;
      if(val == 'administrator'){
        status = 'On Track';
      }else if(val == 'collector'){
        status = 'Pending'
      }
      await this.listaService.createLista(
        {
          listaId: listaId,
          clientId: this.id,
          description: this.description.value,
          alias: this.alias.value,
          status: status,
          totalAmount: this.totalAmount.value,
          startDate: formatDate(this.startDate.value, 'yyyy-MM-dd\'T\'HH:mm:ss', 'en-PH', '+08:00'),
          endDate:formatDate(this.endDate.value, 'yyyy-MM-dd\'T\'HH:mm:ss', 'en-PH', '+08:00'),
          dpa: this.dpa.value,
          amountPaid: '0',
          balance: this.totalAmount.value,
          createdAt: formatDate(new Date(), 'yyyy-MM-dd\'T\'HH:mm:ss', 'en-PH', '+08:00'),
          updatedAt: formatDate(new Date(), 'yyyy-MM-dd\'T\'HH:mm:ss', 'en-PH', '+08:00'),
        }
      ).then(() => {
        this.router.navigate(['menu/client/lista/'+this.id]);
        window.alert('New Client Successfully Created!');
      })
      .catch(error => console.log(error));
    });
    await this.listaService.updateClientListaNo(this.id, {
      lista: listaNo.toString(),
    });
  }

  get description() {
    return this.newListaForm.get('description');
  }

  get alias() {
    return this.newListaForm.get('alias');
  }

  get totalAmount() {
    return this.newListaForm.get('totalAmount');
  }

  get startDate() {
    return this.newListaForm.get('startDate');
  }

  get endDate() {
    return this.newListaForm.get('endDate');
  }

  get dpa() {
    return this.newListaForm.get('dpa');
  }

}
