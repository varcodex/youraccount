import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NewListaPageRoutingModule } from './new-lista-routing.module';

import { NewListaPage } from './new-lista.page';
import { ReactiveFormsModule } from '@angular/forms';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    NewListaPageRoutingModule
  ],
  declarations: [NewListaPage]
})
export class NewListaPageModule {}
