import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { formatDate } from '@angular/common';
import { Collection } from '../../../../../model/client/collection';
import { Lista } from '../../../../../model/client/lista';
import { CollectionService } from '../../../../../service/client/collection.service';
import { ListaService } from '../../../../../service/client/lista.service';
@Component({
  selector: 'app-new-collection',
  templateUrl: './new-collection.page.html',
  styleUrls: ['./new-collection.page.scss'],
})
export class NewCollectionPage implements OnInit {
  id: any;
  listaId: any;
  dateNow: any;
  newCollectionForm: FormGroup;
  balance: number;
  amountPaid: number;
  totalAmount: number;
  status: any;
  constructor(public collectionService: CollectionService,
    public listaService: ListaService,
    private router: Router,
    private formBuilder: FormBuilder,
    private actRoute: ActivatedRoute) { 
      this.id = this.actRoute.snapshot.paramMap.get('id');
      this.listaId = this.actRoute.snapshot.paramMap.get('listaId');
      this.listaService.getListaByID(this.listaId).valueChanges().subscribe((res: Lista) => {
        if(res){
          this.amountPaid = Number(res.amountPaid);
          this.balance = Number(res.balance);
          this.totalAmount = Number(res.totalAmount);
        }
      });
    }

  ngOnInit() {
    this.dateNow = formatDate(new Date(), 'yyyy-MM-dd\'T\'HH:mm:ss', 'en-PH', '+08:00')
    this.newCollectionForm = this.formBuilder.group({
      amount : ['', Validators.required],
    }, {
      updateOn: 'change'
    });
  }

  async createCollection(){
    await this.collectionService.createCollection(
      {
        listaId: this.listaId,
        amount: this.amount.value,
        createdAt: formatDate(new Date(), 'yyyy-MM-dd\'T\'HH:mm:ss', 'en-PH', '+08:00'),
        updatedAt: formatDate(new Date(), 'yyyy-MM-dd\'T\'HH:mm:ss', 'en-PH', '+08:00'),
      }
    ).then(() => {
      this.router.navigate(['menu/client/lista/'+this.id+'/collection/'+this.listaId]);
      window.alert(`You Pay ${this.amount.value} today!`);
    })
    .catch(error => console.log(error));

    if(this.amount.value == '0'){
      this.status = 'Overdue';
      this.collectionService.updateLista(this.listaId, this.amountPaid.toString(),
                                        this.balance.toString(), this.status);
    }else{
      this.balance = this.balance - Number(this.amount.value);
      this.amountPaid = this.amountPaid + Number(this.amount.value);
      if(this.balance == 0 || this.balance < 0){
        this.status = 'Completed'
      }else{
        this.status = 'On Track'
      } 
      this.collectionService.updateLista(this.listaId, this.amountPaid.toString(),
                                        this.balance.toString(), this.status);
    }
  }

  get amount() {
    return this.newCollectionForm.get('amount');
  }

}
