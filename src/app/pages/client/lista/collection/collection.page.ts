import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { Collection } from '../../../../model/client/collection';
import { CollectionService } from '../../../../service/client/collection.service';
import { formatDate } from '@angular/common';

import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;
 
import { File } from '@ionic-native/file/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { Platform } from '@ionic/angular';

import { Storage } from '@ionic/storage';
@Component({
  selector: 'app-collection',
  templateUrl: './collection.page.html',
  styleUrls: ['./collection.page.scss'],
})

export class CollectionPage implements OnInit {
  letterObj = {
    to: '',
    from: '',
    text: ''
  }

  listaId: any;
  id: any;
  collection: Collection[] = [];
  filteredCollection: Collection[] = [];
  startDate: any;
  endDate: any;
  totalAmount: any;
  dpa: any;
  amountPaid: any;
  balance: any;
  clientName: any;
  status: any;
  pdfObj = null;

  constructor(public collectionService: CollectionService,
    private router: Router,
    private actRoute: ActivatedRoute,
    public file: File,
    private plt: Platform,
    public fileOpener: FileOpener,
    private storage: Storage) {
      this.listaId = this.actRoute.snapshot.paramMap.get('listaId');
      this.id = this.actRoute.snapshot.paramMap.get('id');
  }

  async createPdf() {
    this.collection = await this.collectionService.getCollection(this.listaId).toPromise();
    var data = [];
    data.push([{ text: 'DATE', bold: true }, { text: 'AMOUNT', bold: true }]);
    for (const c of this.collection) {
      const date = formatDate(c.createdAt, 'EEEE, MMMM d, y', 'en-PH', '+08:00');
      data.push([{text: date}, {text: c.amount}]);
    }

    var docDefinition = {
      pageSize: {
        width: 226.7766533435383,
        height: 'auto'
      },
      pageMargins: [ 10, 10, 10, 10 ],
      content: [
        { text: 'Client Name: '+ this.clientName +', Start Date: '+ formatDate(this.startDate , 'EEEE, MMMM d, y', 'en-PH', '+08:00') +', End Date: '+ formatDate(this.endDate , 'EEEE, MMMM d, y', 'en-PH', '+08:00') +', Total Amount Lent: '+ this.totalAmount +', Daily Payment Amount: '+ this.dpa +', Amount Paid: '+ this.amountPaid +', Balance: '+ this.balance, alignment: 'center' },
        { text: ' ', alignment: 'center' },
        { text: 'Collection History', alignment: 'center', bold: true  },
        { text: ' ', alignment: 'center' },
        {
          layout: 'lightHorizontalLines',
          table: {
            // headers are automatically repeated if the table spans over multiple pages
            // you can declare how many rows should be treated as headers
            headerRows: 1,
            widths: [ 'auto', 'auto' ],
            body: data
          }
        }
      ],
      defaultStyle: {
        fontSize: 16
      },
      styles: {
        header: {
          fontSize: 24,
          bold: true,
        },
        subheader: {
          fontSize: 20,
          bold: true,
          margin: [0, 0, 0, 0]
        },
        story: {
          italic: true,
          alignment: 'center',
          width: '50%',
        }
      }
    }
    this.pdfObj = pdfMake.createPdf(docDefinition);
  }
  
  async ngOnInit() {
    this.collection = await this.collectionService.getCollection(this.listaId).toPromise();
    await this.storage.get('startDate').then((val) => {
      this.startDate = val;
    });
    await this.storage.get('endDate').then((val) => {
      this.endDate = val;
    });
    await this.storage.get('totalAmount').then((val) => {
      this.totalAmount = val;
    });
    await this.storage.get('dpa').then((val) => {
      this.dpa = val;
    });
    await this.storage.get('amountPaid').then((val) => {
      this.amountPaid = val;
    });
    await this.storage.get('balance').then((val) => {
      this.balance = val;
    });
    await this.storage.get('clientName').then((val) => {
      this.clientName = val;
    });
    await this.storage.get('status').then((val) => {
      this.status = val;
    });
  }

  deleteCollection(id, amount){
    let curBalance = Number(this.balance) + Number(amount);
    let curAmountPaid = Number(this.amountPaid) - Number(amount);
    if (window.confirm('Do you really want to delete?')) {
      this.collectionService.deleteCollection(id).then(() => {
        window.alert('Successfully Deleted!');
        this.collectionService.updateLista(this.listaId, curAmountPaid.toString(), curBalance.toString(), this.status);
        this.collection.shift();
        this.amountPaid = curAmountPaid;
        this.balance = curBalance;
      })
      .catch(error => console.log(error));
    }
  }

  async doRefresh(event) {  
    setTimeout(async () => {
      this.collection = await this.collectionService.getCollection(this.listaId).toPromise();
      event.target.complete();
      }, 1500); 
  }

  downloadPdf() {
    if (this.plt.is('cordova')) {
      this.pdfObj.getBuffer((buffer) => {
        var blob = new Blob([buffer], { type: 'application/pdf' });
 
        // Save the PDF to the data Directory of our App
        this.file.writeFile(this.file.dataDirectory, this.clientName+'.pdf', blob, { replace: true }).then(fileEntry => {
          // Open the PDf with the correct OS tools
          this.fileOpener.open(this.file.dataDirectory + this.clientName+'.pdf', 'application/pdf');
        })
      });
    } else {
      // On a browser simply use download!
      this.pdfObj.download();
    }
  }

}
