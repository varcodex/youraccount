import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EditListaPage } from './edit-lista.page';

describe('EditListaPage', () => {
  let component: EditListaPage;
  let fixture: ComponentFixture<EditListaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditListaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EditListaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
