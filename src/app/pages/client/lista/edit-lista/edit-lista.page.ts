import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { formatDate } from '@angular/common';
import { Lista } from '../../../../model/client/lista';
import { ListaService } from '../../../../service/client/lista.service';

@Component({
  selector: 'app-edit-lista',
  templateUrl: './edit-lista.page.html',
  styleUrls: ['./edit-lista.page.scss'],
})
export class EditListaPage implements OnInit {
  id: any;
  editListaForm: FormGroup;
  listaId: any;
  constructor(public listaService: ListaService,
    private router: Router,
    private formBuilder: FormBuilder,
    private actRoute: ActivatedRoute) { 
      this.id = this.actRoute.snapshot.paramMap.get('id');
      this.listaId = this.actRoute.snapshot.paramMap.get('listaId');
      this.listaService.getListaByID(this.listaId).valueChanges().subscribe((res: Lista) => {
        if(res){
          this.editListaForm.setValue({
            description: res.description,
            alias: res.alias,
            totalAmount: res.totalAmount,
            startDate: res.startDate,
            endDate: res.endDate,
            dpa: res.dpa,
            amountPaid: res.amountPaid,
            balance: res.balance,
          });
        }
      });
    }

  ngOnInit() {
    this.editListaForm = this.formBuilder.group({
      description: ['', Validators.required],
      alias : ['', Validators.required],
      totalAmount: ['', Validators.required],
      startDate : ['', Validators.required],
      endDate : ['', Validators.required],
      dpa: ['', Validators.required],
      amountPaid: [''],
      balance: [''],
    }, {
      updateOn: 'change'
    });
  }
  
  async editLista() {
    await this.listaService.updateLista(this.listaId, {
        description: this.description.value,
        alias: this.alias.value,
        totalAmount: this.totalAmount.value,
        startDate: formatDate(this.startDate.value, 'yyyy-MM-dd\'T\'HH:mm:ss', 'en-PH', '+08:00'),
        endDate:formatDate(this.endDate.value, 'yyyy-MM-dd\'T\'HH:mm:ss', 'en-PH', '+08:00'),
        dpa: this.dpa.value,
        amountPaid: this.amountPaid.value,
        balance: this.balance.value,
        updatedAt: formatDate(new Date(), 'yyyy-MM-dd\'T\'HH:mm:ss', 'en-PH', '+08:00'),
    })
      .then(() => {
        this.router.navigate(['menu/client/lista/'+this.id]);
        window.alert('Client Successfully Updated!');
      })
      .catch(error => console.log(error));
  }


  get description() {
    return this.editListaForm.get('description');
  }

  get alias() {
    return this.editListaForm.get('alias');
  }

  get totalAmount() {
    return this.editListaForm.get('totalAmount');
  }

  get startDate() {
    return this.editListaForm.get('startDate');
  }

  get endDate() {
    return this.editListaForm.get('endDate');
  }

  get dpa() {
    return this.editListaForm.get('dpa');
  }

  get amountPaid() {
    return this.editListaForm.get('amountPaid');
  }

  get balance() {
    return this.editListaForm.get('balance');
  }

}
