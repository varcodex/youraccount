import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditListaPageRoutingModule } from './edit-lista-routing.module';

import { EditListaPage } from './edit-lista.page';
import { ReactiveFormsModule } from '@angular/forms';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    EditListaPageRoutingModule
  ],
  declarations: [EditListaPage]
})
export class EditListaPageModule {}
