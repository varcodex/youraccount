import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditListaPage } from './edit-lista.page';

const routes: Routes = [
  {
    path: '',
    component: EditListaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditListaPageRoutingModule {}
