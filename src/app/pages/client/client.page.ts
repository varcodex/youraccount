import { Component, OnInit } from '@angular/core';
import { ClientsService } from '../../service/client/clients.service';
import { Clients } from '../../model/client/clients';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-client',
  templateUrl: './client.page.html',
  styleUrls: ['./client.page.scss'],
})
export class ClientPage implements OnInit {
  clients: Clients[] = [];
  filteredClients: Clients[] = [];
  uid: any;
  role: any;
  constructor(public clientsService: ClientsService,
              private router: Router,
              private storage: Storage) { }

  async ngOnInit() {
    await this.storage.get('uid').then((val) => {
      this.uid = val;
    });
    await this.storage.get('role').then(async (val) => {
      this.role = val;
    });
    this.clients = await this.clientsService.getClients(this.uid).toPromise();
  }

  deleteClient(id) {
    if (window.confirm('Do you really want to delete?')) {
      this.clientsService.deleteClient(id).then(() => {
        window.alert('Client Successfully Deleted!');
        this.router.navigate(['menu/client']);
      })
      .catch(error => console.log(error));
    }
    this.clients.shift();
  }

  async doRefresh(event) {
    setTimeout(async () => {
      this.clients = await this.clientsService.getClients(this.uid).toPromise();
      for (const client of this.clients) {
        if (client.id) {
            const clientAvatar = await this.clientsService.getClientAvatar(client.id);
            if (clientAvatar) {
              client.clientAvatarUrl = await clientAvatar.getDownloadURL();
            }else{
              client.clientAvatarUrl = `assets/imgs/blank-avatar.jpg`;
            }
        }
      }
      event.target.complete();
      }, 1500);
  }

}
