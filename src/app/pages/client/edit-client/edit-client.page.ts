import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ClientsService } from '../../../service/client/clients.service';
import { Clients } from '../../../model/client/clients';
import { formatDate } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit-client',
  templateUrl: './edit-client.page.html',
  styleUrls: ['./edit-client.page.scss'],
})
export class EditClientPage implements OnInit {
  editClientForm: FormGroup;
  clients: Clients;
  id: any;
  constructor(public clientsService: ClientsService,
              private formBuilder: FormBuilder,
              private router: Router,
              private actRoute: ActivatedRoute) {
                this.id = this.actRoute.snapshot.paramMap.get('id');
                this.clientsService.getClientByID(this.id).valueChanges().subscribe((res: Clients) => {
                  if (res){
                    this.editClientForm.setValue({
                      firstName: res.firstName,
                      lastName: res.lastName,
                      address: res.address,
                      phoneNumber: res.phoneNumber,
                      gender: res.gender,
                    });
                  }
                });
                this.editClientForm = this.formBuilder.group({
                  firstName: ['', Validators.required],
                  lastName : [''],
                  address: ['', Validators.required],
                  phoneNumber: [''],
                  gender: [Validators.required]
                }, {
                  updateOn: 'change'
                });
            }

  ngOnInit() {}

  async editclient() {
    await this.clientsService.updateClient(this.id, {
      firstName: this.firstName.value,
      lastName: this.lastName.value,
      address: this.address.value,
      phoneNumber: this.phoneNumber.value,
      gender: this.gender.value,
      updatedAt: formatDate(new Date(), 'yyyy-MM-dd\'T\'HH:mm:ss', 'en-PH', '+08:00'),
    })
      .then(() => {
        this.router.navigate(['menu/client']);
        window.alert('Client Successfully Updated!');
      })
      .catch(error => console.log(error));
  }

  get firstName() {
    return this.editClientForm.get('firstName');
  }

  get lastName() {
    return this.editClientForm.get('lastName');
  }

  get address() {
    return this.editClientForm.get('address');
  }

  get phoneNumber() {
    return this.editClientForm.get('phoneNumber');
  }

  get gender() {
    return this.editClientForm.get('gender');
  }

}
