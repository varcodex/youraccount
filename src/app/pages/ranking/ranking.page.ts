import { Component, OnInit } from '@angular/core';
import { Ranking } from '../../model/ranking/ranking';
import { RankingService } from '../../service/ranking/ranking.service';
import { ClientsService } from '../../service/client/clients.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-ranking',
  templateUrl: './ranking.page.html',
  styleUrls: ['./ranking.page.scss'],
})
export class RankingPage implements OnInit {
  listItems: any;
  uid: any;
  ranking: Ranking[] = [];
  collectorId: any;

  constructor(public rankingService: RankingService,
              public clientsService: ClientsService,
              private storage: Storage) {}

  async ngOnInit() {
    await this.storage.get('uid').then((val) => {
      this.uid = val;
    });
    this.ranking = await this.rankingService.startCollection(this.uid).toPromise();
  }

  onRenderItems(event) {
    console.log(`Moving item from ${event.detail.from} to ${event.detail.to}`);
    const draggedItem = this.ranking.splice(event.detail.from, 1)[0];
    this.ranking.splice(event.detail.to, 0, draggedItem);
    // this.listItems = reorderArray(this.listItems, event.detail.from, event.detail.to);
    event.detail.complete();
  }

  getList() {
    const clientId: any[] = [];
    for (const r of this.ranking){
      clientId.push(r.clientId);
      this.collectorId = r.collectorId;
    }
    const result = this.rankingService.updateRanking(this.collectorId, clientId);
    if (result){
      alert('Ranking updated!');
    }
  }

}
