import { Component, OnInit } from '@angular/core';
import { ClientsService } from '../../service/client/clients.service';
import { Notification } from '../../model/notification/notification';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
@Component({
  selector: 'app-notification',
  templateUrl: './notification.page.html',
  styleUrls: ['./notification.page.scss'],
})
export class NotificationPage implements OnInit {
  clients: Notification[] = [];
  filteredClients: Notification[] = [];
  uid: any;
  constructor(public clientsService: ClientsService,
              private router: Router,
              private storage: Storage) { }

  async ngOnInit() {
    this.clients = await this.clientsService.getNotification().toPromise();
  }

  async doApprove(clientId, listaId){
    await this.clientsService.updateListaStatus(listaId, 'On Track');
    await this.clientsService.updateClientStatus(clientId, 'active');
    await this.clients.shift();
  }

  async doReject(clientId, listaId, clientStatus){
    await this.clientsService.updateListaStatus(listaId, 'Rejected');
    await this.clientsService.updateClientStatus(clientId, clientStatus);
    await this.clients.shift();
  }

  async doRefresh(event) {
    setTimeout(async () => {
      this.clients = await this.clientsService.getNotification().toPromise();
      for (const client of this.clients) {
        if (client.id) {
            const clientAvatar = await this.clientsService.getClientAvatar(client.id);
            if (clientAvatar) {
              client.clientAvatarUrl = await clientAvatar.getDownloadURL();
            }
        }
      }
      event.target.complete();
      }, 1500);
  }

}
