import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { formatDate } from '@angular/common';
import { Collection } from '../../model/client/collection';
import { CollectionService } from '../../service/collection/collection.service';
import { ClientsService } from '../../service/client/clients.service';
@Component({
  selector: 'app-collection',
  templateUrl: './collection.page.html',
  styleUrls: ['./collection.page.scss'],
})
export class CollectionPage implements OnInit {
  dateNow: any;
  uid: any;
  tempDateNow: any;
  collection: Collection[] = [];
  todayCollection: Collection[] = [];
  todayDate: Date = new Date();
  totalCollection: any;

  constructor(public collectionService: CollectionService,
              public clientsService: ClientsService,
              private storage: Storage) {
      this.tempDateNow = formatDate(new Date(), 'yyyy-MM-dd', 'en-PH', '+08:00');
    }

  async ngOnInit() {
    await this.storage.get('dateNow').then((val) => {
      this.dateNow = val;
    });
    await this.storage.get('uid').then((val) => {
      this.uid = val;
    });
    if (this.dateNow == null || this.dateNow !== this.tempDateNow){
      this.dateNow = formatDate(new Date(), 'yyyy-MM-dd', 'en-PH', '+08:00');
      await this.storage.set('dateNow', this.dateNow);
      await this.storage.set('totalCollection', '0');
      await this.storage.get('totalCollection').then((val) => {
        this.totalCollection = val;
      });
      this.collection = await this.collectionService.startCollection(this.uid).toPromise();
      await this.storage.set('todayCollection', JSON.stringify(this.collection));
      await this.storage.get('todayCollection').then((val) => {
        console.log('new date');
        this.todayCollection = JSON.parse(val);
      });
    }else{
      this.collection = await this.collectionService.startCollection(this.uid).toPromise();
      await this.storage.get('todayCollection').then((val) => {
        console.log('outdated');
        this.todayCollection = JSON.parse(val);
      });
      await this.storage.get('totalCollection').then((val) => {
        this.totalCollection = val;
      });
    }
  }

  async notPaidButton(listaId, balance, amountPaid){
      await this.collectionService.createCollection(
        {
          listaId,
          amount: '0',
          createdAt: formatDate(new Date(), 'yyyy-MM-dd\'T\'HH:mm:ss', 'en-PH', '+08:00'),
          updatedAt: formatDate(new Date(), 'yyyy-MM-dd\'T\'HH:mm:ss', 'en-PH', '+08:00'),
        }
      ).then(() => {
      })
      .catch(error => console.log(error));

      await this.collectionService.updateLista(listaId, amountPaid,
      balance, 'Overdue');
      await this.todayCollection.shift();
      await this.storage.set('todayCollection', JSON.stringify(this.todayCollection));
  }

  async PaidByDPAButton(listaId, balance, amount, amountPaid){
    let status: any;
    let curBalance: any;
    let curAmountPaid: any;
    let tAmount: any;
    await this.collectionService.createCollection(
        {
          listaId,
          amount,
          createdAt: formatDate(new Date(), 'yyyy-MM-dd\'T\'HH:mm:ss', 'en-PH', '+08:00'),
          updatedAt: formatDate(new Date(), 'yyyy-MM-dd\'T\'HH:mm:ss', 'en-PH', '+08:00'),
        }
      ).then(() => {
      })
      .catch(error => console.log(error));

    curBalance = Number(balance) - Number(amount);
    curAmountPaid = Number(amountPaid) + Number(amount);
    await this.storage.get('totalCollection').then((val) => {
        this.totalCollection = val;
      });
    tAmount = Number(this.totalCollection) + Number(amount);
    await this.storage.set('totalCollection', tAmount.toString());
    if (curBalance === 0 || curBalance < 0){
        status = 'Completed';
      }else{
        status = 'On Track';
      }
    await this.collectionService.updateLista(listaId, curAmountPaid.toString(),
        curBalance.toString(), status);
    await this.todayCollection.shift();
    await this.storage.set('todayCollection', JSON.stringify(this.todayCollection));
  }

  async PaidByAmountButton(listaId, balance, amount, amountPaid){
    let status: any;
    let curBalance: any;
    let curAmountPaid: any;
    let tAmount: any;
    if (amount.value){
      await this.collectionService.createCollection(
        {
          listaId,
          amount: amount.value,
          createdAt: formatDate(new Date(), 'yyyy-MM-dd\'T\'HH:mm:ss', 'en-PH', '+08:00'),
          updatedAt: formatDate(new Date(), 'yyyy-MM-dd\'T\'HH:mm:ss', 'en-PH', '+08:00'),
        }
      ).then(() => {
      })
      .catch(error => console.log(error));

      curBalance = Number(balance) - Number(amount.value);
      curAmountPaid = Number(amountPaid) + Number(amount.value);
      await this.storage.get('totalCollection').then((val) => {
        this.totalCollection = val;
      });
      tAmount = Number(this.totalCollection) + Number(amount.value);
      await this.storage.set('totalCollection', tAmount.toString());
      if (curBalance === 0 || curBalance < 0){
        status = 'Completed';
      }else{
        status = 'On Track';
      }
      await this.collectionService.updateLista(listaId, curAmountPaid.toString(),
        curBalance.toString(), status);
      await this.todayCollection.shift();
      await this.storage.set('todayCollection', JSON.stringify(this.todayCollection));
    }else{
      window.alert(`Invalid amount, please enter the valid amount!`);
    }

  }

  async doRefresh(event) {
    setTimeout(async () => {
      await this.storage.get('dateNow').then((val) => {
        this.dateNow = val;
      });
      await this.storage.get('uid').then((val) => {
        this.uid = val;
      });
      if (this.dateNow == null || this.dateNow !== this.tempDateNow){
        this.dateNow = formatDate(new Date(), 'yyyy-MM-dd', 'en-PH', '+08:00');
        await this.storage.set('dateNow', this.dateNow);
        await this.storage.set('totalCollection', '0');
        await this.storage.get('totalCollection').then((val) => {
          this.totalCollection = val;
        });
        this.collection = await this.collectionService.startCollection(this.uid).toPromise();
        await this.storage.set('todayCollection', JSON.stringify(this.collection));
        await this.storage.get('todayCollection').then((val) => {
          console.log('new date');
          this.todayCollection = JSON.parse(val);
        });
      }else{
        this.collection = await this.collectionService.startCollection(this.uid).toPromise();
        await this.storage.get('todayCollection').then((val) => {
          console.log('outdated');
          this.todayCollection = JSON.parse(val);
        });
        await this.storage.get('totalCollection').then((val) => {
          this.totalCollection = val;
        });
      }
      for (const client of this.todayCollection) {
        if (client.id) {
            const clientAvatar = await this.clientsService.getClientAvatar(client.id);
            if (clientAvatar) {
              client.clientAvatarUrl = await clientAvatar.getDownloadURL();
            }else{
              client.clientAvatarUrl = `assets/imgs/blank-avatar.jpg`;
            }
        }
      }
      event.target.complete();
      }, 1500);
  }
}
