import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { AuthService } from '../../service/auth/auth.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.page.html',
  styleUrls: ['./forgot-password.page.scss'],
})
export class ForgotPasswordPage implements OnInit {
  imgSrc : String = '/assets/imgs/logo.png';
  
  constructor(
    public authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  onLogin(){
    this.router.navigate(['login']);
  }

  async onforgotPassword(email){
    await this.authService.forgotPassword(email.value);
  }

}
