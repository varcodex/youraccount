import { Component, OnInit } from '@angular/core';
import { ClientsService } from '../../../service/client/clients.service';
import { CollectorService } from '../../../service/collector/collector.service';
import { Clients } from '../../../model/client/clients';
import { ActivatedRoute } from "@angular/router";
import { formatDate } from '@angular/common';
@Component({
  selector: 'app-assign-client',
  templateUrl: './assign-client.page.html',
  styleUrls: ['./assign-client.page.scss'],
})
export class AssignClientPage implements OnInit {
  clients: Clients[] = [];
  filteredClients: Clients[] = [];
  id: any;
  collectorId: string;
  status = 'Select All';
  constructor(public collectorService: CollectorService,
              public clientsService: ClientsService,
              private actRoute: ActivatedRoute) {
      this.id = this.actRoute.snapshot.paramMap.get('id');
      this.collectorId = this.actRoute.snapshot.paramMap.get('collectorId');
    }

  async ngOnInit() {
    this.clients = await this.clientsService.getAssignClient(this.collectorId).toPromise();
  }

  changeStatus(){
    if (this.status === 'Select All'){
      for (const c of this.clients){
        c.is_assigned = true;
      }
      this.status = 'Unselect All';
    }else{
      for (const c of this.clients){
        c.is_assigned = false;
      }
      this.status = 'Select All';
    }
  }
  async doSave(){
    let data: any[] = [];
    for(let client of this.clients){
      if(client.is_assigned){
        data.push(client.id);
        await this.clientsService.updateClient(client.id, {
          collectorId: this.collectorId,
          updatedAt: formatDate(new Date(), 'yyyy-MM-dd\'T\'HH:mm:ss', 'en-PH', '+08:00'),
        }).then(async () => {
        });
      }else{
        await this.clientsService.updateClient(client.id, {
          collectorId: '',
          updatedAt: formatDate(new Date(), 'yyyy-MM-dd\'T\'HH:mm:ss', 'en-PH', '+08:00'),
        }).then(async () => {
        });
      }
    }
    await this.collectorService.updateCollector(this.id, {
      clientId: data,
      updatedAt: formatDate(new Date(), 'yyyy-MM-dd\'T\'HH:mm:ss', 'en-PH', '+08:00'),
    }).then(async () => {
      window.alert('Collectors Clients Successfully Updated!');
    });
  }
}
