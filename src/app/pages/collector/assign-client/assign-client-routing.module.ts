import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AssignClientPage } from './assign-client.page';

const routes: Routes = [
  {
    path: '',
    component: AssignClientPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AssignClientPageRoutingModule {}
