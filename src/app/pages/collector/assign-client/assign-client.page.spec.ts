import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AssignClientPage } from './assign-client.page';

describe('AssignClientPage', () => {
  let component: AssignClientPage;
  let fixture: ComponentFixture<AssignClientPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignClientPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AssignClientPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
