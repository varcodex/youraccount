import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AssignClientPageRoutingModule } from './assign-client-routing.module';

import { AssignClientPage } from './assign-client.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AssignClientPageRoutingModule
  ],
  declarations: [AssignClientPage]
})
export class AssignClientPageModule {}
