import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CollectorPage } from './collector.page';

const routes: Routes = [
  {
    path: '',
    component: CollectorPage
  },
  {
    path: 'new-collector',
    loadChildren: () => import('./new-collector/new-collector.module').then( m => m.NewCollectorPageModule)
  },
  {
    path: 'edit-collector/:id',
    loadChildren: () => import('./edit-collector/edit-collector.module').then( m => m.EditCollectorPageModule)
  },
  {
    path: 'assign-client/:id/:collectorId',
    loadChildren: () => import('./assign-client/assign-client.module').then( m => m.AssignClientPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CollectorPageRoutingModule {}
