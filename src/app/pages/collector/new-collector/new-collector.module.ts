import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NewCollectorPageRoutingModule } from './new-collector-routing.module';

import { NewCollectorPage } from './new-collector.page';
import { ReactiveFormsModule } from '@angular/forms';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    NewCollectorPageRoutingModule
  ],
  declarations: [NewCollectorPage]
})
export class NewCollectorPageModule {}
