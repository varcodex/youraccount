import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NewCollectorPage } from './new-collector.page';

describe('NewCollectorPage', () => {
  let component: NewCollectorPage;
  let fixture: ComponentFixture<NewCollectorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewCollectorPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NewCollectorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
