import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewCollectorPage } from './new-collector.page';

const routes: Routes = [
  {
    path: '',
    component: NewCollectorPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewCollectorPageRoutingModule {}
