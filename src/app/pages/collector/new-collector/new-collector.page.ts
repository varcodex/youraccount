import { Component, OnInit } from '@angular/core';
import { Collector } from '../../../model/collector/collector';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { formatDate } from '@angular/common';
import { Base64 } from '@ionic-native/base64/ngx';
import { Router } from '@angular/router';
import { CollectorService } from '../../../service/collector/collector.service';
import { AuthService } from '../../../service/auth/auth.service';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
@Component({
  selector: 'app-new-collector',
  templateUrl: './new-collector.page.html',
  styleUrls: ['./new-collector.page.scss'],
})
export class NewCollectorPage implements OnInit {
  newCollectorForm: FormGroup;
  collector: Collector;
  collectorId: any;
  clientNoDoc: any;
  imgPreview = 'assets/imgs/blank-avatar.jpg';
  imageResponse: any;
  options: any;
  capturedSnapURL: string;
  constructor(public collectorService: CollectorService,
              private authService: AuthService,
              private formBuilder: FormBuilder,
              private router: Router) {}

  async ngOnInit() {
    this.newCollectorForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName : [''],
      address: ['', Validators.required],
      phoneNumber: [''],
      gender: [Validators.required]
    }, {
      updateOn: 'change'
    });
  }

  async createCollector() {
    try {
      const userCredential = await this.authService.signUp(this.email.value, this.password.value);

      await this.collectorService.updateWithUploadAvataCollector(this.authService.createId(), {
        firstName: this.firstName.value,
        lastName: this.lastName.value,
        address: this.address.value,
        phoneNumber: this.phoneNumber.value,
        gender: this.gender.value,
        clientId: [],
        uuid: userCredential.user.uid,
        status: 'Active',
        role: 'collector',
        createdAt: formatDate(new Date(), 'yyyy-MM-dd\'T\'HH:mm:ss', 'en-PH', '+08:00'),
        updatedAt: formatDate(new Date(), 'yyyy-MM-dd\'T\'HH:mm:ss', 'en-PH', '+08:00'),
      }).then(async () => {
        this.router.navigate(['menu/collector']);
        window.alert('New Collector Successfully Created!');
      });
    } catch (error) {
      console.log(error);
      await alert('The email you\'ve entered is already registered');
    }
  }

  get firstName() {
    return this.newCollectorForm.get('firstName');
  }

  get lastName() {
    return this.newCollectorForm.get('lastName');
  }

  get address() {
    return this.newCollectorForm.get('address');
  }

  get phoneNumber() {
    return this.newCollectorForm.get('phoneNumber');
  }

  get gender() {
    return this.newCollectorForm.get('gender');
  }
  get email() {
    return this.newCollectorForm.get('email');
  }

  get password() {
    return this.newCollectorForm.get('password');
  }

}
