import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditCollectorPageRoutingModule } from './edit-collector-routing.module';

import { EditCollectorPage } from './edit-collector.page';
import { ReactiveFormsModule } from '@angular/forms';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    EditCollectorPageRoutingModule
  ],
  declarations: [EditCollectorPage]
})
export class EditCollectorPageModule {}
