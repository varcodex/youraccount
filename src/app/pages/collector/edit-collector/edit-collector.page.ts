import { Component, OnInit } from '@angular/core';
import { Collector } from '../../../model/collector/collector';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { formatDate } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { CollectorService } from '../../../service/collector/collector.service';

@Component({
  selector: 'app-edit-collector',
  templateUrl: './edit-collector.page.html',
  styleUrls: ['./edit-collector.page.scss'],
})
export class EditCollectorPage implements OnInit {
  editCollectorForm: FormGroup;
  collector: Collector;
  collectorId: any;
  clientNoDoc: any;
  imgPreview = 'assets/imgs/blank-avatar.jpg';
  imageResponse: any;
  options: any;
  id: any;
  currentImage: any;
  constructor(public collectorService: CollectorService,
              private formBuilder: FormBuilder,
              private router: Router,
              private actRoute: ActivatedRoute) {
      this.id = this.actRoute.snapshot.paramMap.get('id');
      this.currentImage = `assets/imgs/blank-avatar.jpg`;
      this.collectorService.getCollectorByID(this.id).valueChanges().subscribe((res: Collector) => {
        if (res){
          this.editCollectorForm.setValue({
            firstName: res.firstName,
            lastName: res.lastName,
            address: res.address,
            phoneNumber: res.phoneNumber,
            gender: res.gender,
          });
        }
      });
      this.editCollectorForm = this.formBuilder.group({
        firstName: ['', Validators.required],
        lastName : [''],
        address: ['', Validators.required],
        phoneNumber: [''],
        gender: [Validators.required]
      }, {
        updateOn: 'change'
      });
    }

  async ngOnInit() {
    const clientAvatar = await this.collectorService.getCollectorAvatar(this.id);
    if (clientAvatar) {
      this.currentImage = await clientAvatar.getDownloadURL();
    }
  }

  async editCollector() {
      await this.collectorService.updateCollector(this.id, {
        firstName: this.firstName.value,
        lastName: this.lastName.value,
        address: this.address.value,
        phoneNumber: this.phoneNumber.value,
        gender: this.gender.value,
        updatedAt: formatDate(new Date(), 'yyyy-MM-dd\'T\'HH:mm:ss', 'en-PH', '+08:00'),
      }).then(async () => {
        this.router.navigate(['menu/collector']);
        window.alert('Collector Successfully Updated!');
      });
  }

  get firstName() {
    return this.editCollectorForm.get('firstName');
  }

  get lastName() {
    return this.editCollectorForm.get('lastName');
  }

  get address() {
    return this.editCollectorForm.get('address');
  }

  get phoneNumber() {
    return this.editCollectorForm.get('phoneNumber');
  }

  get gender() {
    return this.editCollectorForm.get('gender');
  }
}
