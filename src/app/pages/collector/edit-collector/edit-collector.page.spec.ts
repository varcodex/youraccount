import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EditCollectorPage } from './edit-collector.page';

describe('EditCollectorPage', () => {
  let component: EditCollectorPage;
  let fixture: ComponentFixture<EditCollectorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditCollectorPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EditCollectorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
