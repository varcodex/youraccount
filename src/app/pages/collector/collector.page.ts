import { Component, OnInit } from '@angular/core';
import { CollectorService } from '../../service/collector/collector.service';
import { Collector } from '../../model/collector/collector';
import { Router } from '@angular/router';

@Component({
  selector: 'app-collector',
  templateUrl: './collector.page.html',
  styleUrls: ['./collector.page.scss'],
})
export class CollectorPage implements OnInit {
  collectors: Collector[] = [];
  filteredCollectors: Collector[] = [];

  constructor(public collectorService: CollectorService,
              private router: Router) { }

  async ngOnInit() {
    this.collectors = await this.collectorService.getCollectors().toPromise();
  }

  async doRefresh(event) {
    setTimeout(async () => {
      this.collectors = await this.collectorService.getCollectors().toPromise();
      event.target.complete();
      }, 1500);
  }

  deleteCollector(id) {
    console.log(id);
    if (window.confirm('Do you really want to delete?')) {
      this.collectorService.deleteCollector(id).then(() => {
        window.alert('Collector Successfully Deleted!');
        this.router.navigate(['menu/collector']);
      })
      .catch(error => console.log(error));
    }
    this.collectors.shift();
  }

}
