import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { AuthService } from "../../service/auth/auth.service";
import { Storage } from '@ionic/storage';
import { CollectorService } from "../../service/collector/collector.service";
import { Collector } from '../../model/collector/collector';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  imgSrc : String = '/assets/imgs/logo.png';
  collector: Collector[] = [];
  role: Collector[] = [];
  constructor(
    public authService: AuthService,
    public CollectorService: CollectorService,
    public router: Router,
    private storage: Storage
  ) { }

  ngOnInit() {}

  async onlogIn(email, password) {
    await this.authService.SignIn(email.value, password.value)
      .then(async (res) => {
          await this.storage.set('uid', res.user.uid);
          this.collector = await this.CollectorService.getCollector(res.user.uid).toPromise();
          this.role = await this.CollectorService.getRole(res.user.uid).toPromise();
          
          for (const c of this.collector) {
            await this.storage.set('collectorId', c.collectorId);
          }
          for (const role of this.role) {
            await this.storage.set('role', role.role);
          }
          this.router.navigate(['menu/client']);          
      }).catch((error) => {
        window.alert(error.message)
      })
  }
  onforgotPassword(){
    this.router.navigate(['forgot-password']);
  }

}
