export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: "AIzaSyCUdHVIM93kGNuQ5sGvUJcg6LrWHn95gmg",
    authDomain: "your-account-71e9d.firebaseapp.com",
    databaseURL: "https://your-account-71e9d.firebaseio.com",
    projectId: "your-account-71e9d",
    storageBucket: "your-account-71e9d.appspot.com",
    messagingSenderId: "145340134069",
    appId: "1:145340134069:web:3254b8b13e96be969584fc",
    measurementId: "G-653E4B8V8H"
  },
  yaFunctions: {
    url: 'https://us-central1-your-account-71e9d.cloudfunctions.net/youraccount'
  }
};
