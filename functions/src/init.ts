const admin = require('firebase-admin');  
admin.initializeApp({
  credential: admin.credential.cert(require('../your-account-71e9d-firebase-adminsdk-d51pc-4f0b62cb8c.json')),
  databaseURL: "https://your-account-71e9d.firebaseio.com"
});

export const db = admin.firestore();