import * as functions from 'firebase-functions';
import { db } from './init';
import * as express from 'express';

const cors = require('cors');
const app = express();
app.use(cors({origin: true}));

app.get('/clients/:id', async (request, response) => {
    const idParam = request.params['id'];
    const clients: any[] = [];
    const collectorDetails = await db.collection(`collector`).where(`uid`, `==`, `${idParam}`).get();
    // return an empty response if no result is found
    if (!collectorDetails || collectorDetails.docs.length === 0) {
        return response.status(200).json(clients);
    }
    const collectorSnaps = collectorDetails.docs;
    if (collectorSnaps && collectorSnaps.length > 0) {
        for (const cs of collectorSnaps) {
            const clientIDField: string[] = cs.get('clientId');
            const role: string = cs.get('role');
            if(role == 'administrator'){
                const clientData = await db.collection(`client`).orderBy('createdAt', 'desc').get();
                // return an empty response if no result is found
                if (!clientData) {
                    return response.status(200).json(clients);
                }
                const cd = clientData.docs;
                for (const c of cd) {
                        const myListaDetails = await db.collection(`lista`).orderBy('createdAt', 'desc').get();
                        const myListaSnaps = myListaDetails.docs;
                        let status = 'inactive';
                        if (myListaSnaps && myListaSnaps.length > 0) {
                            for (const myListaSnap of myListaSnaps) {
                                if(myListaSnap.get('clientId') === c.id){
                                    if(myListaSnap.get('status') !== 'Completed'){
                                        status = 'active'
                                        break;
                                    }
                                }
                            }
                        }
                        clients.push({
                            id: c.id,
                            clientId: c.get('clientId'),
                            firstName: c.get('firstName'),
                            lastName: c.get('lastName'),
                            address: c.get('address'),
                            gender: c.get('gender'),
                            phoneNumber: c.get('phoneNumber'),
                            status: status
                        });
                }
            }else if(role == 'collector'){
                if (clientIDField) {
                    for (const clientID of clientIDField) {
                        const clientData = await db.collection(`client`).orderBy('createdAt', 'desc').get();
                        // return an empty response if no result is found
                        if (!clientData) {
                            return response.status(200).json(clients);
                        }
                        const cd = clientData.docs;
                        if (cd && cd.length > 0) {
                            for (const c of cd) {
                                if(c.id === clientID){
                                    const myListaDetails = await db.collection(`lista`).orderBy('createdAt', 'desc').get();
                                    const myListaSnaps = myListaDetails.docs;
                                    let status = 'inactive';
                                    if (myListaSnaps && myListaSnaps.length > 0) {
                                        for (const myListaSnap of myListaSnaps) {
                                            if(myListaSnap.get('clientId') === clientID){
                                                if(myListaSnap.get('status') !== 'Completed'){
                                                    status = 'active'
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    clients.push({
                                        id: c.id,
                                        clientId: c.get('clientId'),
                                        firstName: c.get('firstName'),
                                        lastName: c.get('lastName'),
                                        address: c.get('address'),
                                        gender: c.get('gender'),
                                        phoneNumber: c.get('phoneNumber'),
                                        status: status
                                    });
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return response.status(200).json(clients);
});

app.get('/notification', async (request, response) => {
    const notification: any[] = [];
    const clientData = await db.collection(`client`).orderBy('createdAt', 'desc').get();
    // return an empty response if no result is found
    if (!clientData) {
        return response.status(200).json(notification);
    }
    const cd = clientData.docs;
    for (const c of cd) {
        const myListaDetails = await db.collection(`lista`).orderBy('createdAt', 'desc').get();
        const myListaSnaps = myListaDetails.docs;
        let status;
        if (myListaSnaps && myListaSnaps.length > 0) {
            for (const myListaSnap of myListaSnaps) {
                if(myListaSnap.get('clientId') === c.id){
                    if(myListaSnap.get('status') == 'Pending'){
                        status = 'pending';
                        notification.push({
                            id: c.id,
                            clientId: c.get('clientId'),
                            firstName: c.get('firstName'),
                            lastName: c.get('lastName'),
                            address: c.get('address'),
                            gender: c.get('gender'),
                            phoneNumber: c.get('phoneNumber'),
                            clientStatus: c.get('status'),
                            listaStatus: status,
                            myListaSnapId: myListaSnap.id,
                            listaId: myListaSnap.get('listaId'),
                            description: myListaSnap.get('description'),
                            alias: myListaSnap.get('alias'),
                            totalAmount: myListaSnap.get('totalAmount'),
                            startDate: myListaSnap.get('startDate'),
                            endDate: myListaSnap.get('endDate'),
                            dpa: myListaSnap.get('dpa'),
                            amountPaid: myListaSnap.get('amountPaid'),
                            balance: myListaSnap.get('balance'),
                            createdAt: myListaSnap.get('createdAt'),
                            updatedAt: myListaSnap.get('updatedAt')
                        });
                        break;
                    }
                }
            }
        }
    }
    return response.status(200).json(notification);
});

app.get('/getRole/:id', async (request, response) => {
    const idParam = request.params['id'];
    const collector: any[] = [];
    const collectorDetails = await db.collection(`collector`).where(`uid`, `==`, `${idParam}`).get();
    // return an empty response if no result is found
    if (!collectorDetails || collectorDetails.docs.length === 0) {
        return response.status(200).json(collector);
    }
    const collectorSnaps = collectorDetails.docs;
    if (collectorSnaps && collectorSnaps.length > 0) {
        for (const cs of collectorSnaps) {
                collector.push({
                    role: cs.get('role')
                });
        }
    }
    return response.status(200).json(collector);
});

app.get('/clientOntrack/:id', async (request, response) => {
    const idParam = request.params['id'];
    const clients: any[] = [];
    const collectorDetails = await db.collection(`collector`).where(`uid`, `==`, `${idParam}`).get();
    // return an empty response if no result is found
    if (!collectorDetails || collectorDetails.docs.length === 0) {
        return response.status(200).json(clients);
    }
    const collectorSnaps = collectorDetails.docs;
    if (collectorSnaps && collectorSnaps.length > 0) {
        for (const cs of collectorSnaps) {
            const clientIDField: string[] = cs.get('clientId');
            const role: string = cs.get('role');
            if(role == 'administrator'){
                const clientData = await db.collection(`client`).orderBy('createdAt', 'desc').get();
                // return an empty response if no result is found
                if (!clientData) {
                    return response.status(200).json(clients);
                }
                const cd = clientData.docs;
                if (cd && cd.length > 0) {
                    for (const c of cd) {
                            const myListaDetails = await db.collection(`lista`).orderBy('createdAt', 'desc').get();
                            const myListaSnaps = myListaDetails.docs;
                            let status;
                            if (myListaSnaps && myListaSnaps.length > 0) {
                                for (const myListaSnap of myListaSnaps) {
                                    if(myListaSnap.get('clientId') === c.id){
                                        if(myListaSnap.get('status') == 'On Track'){
                                            status = 'On Track'
                                            clients.push({
                                                id: c.id,
                                                clientId: c.get('clientId'),
                                                firstName: c.get('firstName'),
                                                lastName: c.get('lastName'),
                                                address: c.get('address'),
                                                gender: c.get('gender'),
                                                phoneNumber: c.get('phoneNumber'),
                                                status: status
                                            });
                                            break;
                                        }
                                    }
                                }
                            }
                    }
                }
            }else if(role == 'collector'){
                if (clientIDField) {
                for (const clientID of clientIDField) {
                    const clientData = await db.collection(`client`).orderBy('createdAt', 'desc').get();
                    // return an empty response if no result is found
                    if (!clientData) {
                        return response.status(200).json(clients);
                    }
                    const cd = clientData.docs;
                    if (cd && cd.length > 0) {
                        for (const c of cd) {
                            if(c.id === clientID){
                                const myListaDetails = await db.collection(`lista`).orderBy('createdAt', 'desc').get();
                                const myListaSnaps = myListaDetails.docs;
                                let status;
                                if (myListaSnaps && myListaSnaps.length > 0) {
                                    for (const myListaSnap of myListaSnaps) {
                                        if(myListaSnap.get('clientId') === clientID){
                                            if(myListaSnap.get('status') == 'On Track'){
                                                status = 'On Track'
                                                clients.push({
                                                    id: c.id,
                                                    clientId: c.get('clientId'),
                                                    firstName: c.get('firstName'),
                                                    lastName: c.get('lastName'),
                                                    address: c.get('address'),
                                                    gender: c.get('gender'),
                                                    phoneNumber: c.get('phoneNumber'),
                                                    status: status
                                                });
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            }
        }
    }
    return response.status(200).json(clients);
});

app.get('/clientOverdue/:id', async (request, response) => {
    const idParam = request.params['id'];
    const clients: any[] = [];
    const collectorDetails = await db.collection(`collector`).where(`uid`, `==`, `${idParam}`).get();
    // return an empty response if no result is found
    if (!collectorDetails || collectorDetails.docs.length === 0) {
        return response.status(200).json(clients);
    }
    const collectorSnaps = collectorDetails.docs;
    if (collectorSnaps && collectorSnaps.length > 0) {
        for (const cs of collectorSnaps) {
            const clientIDField: string[] = cs.get('clientId');
            const role: string = cs.get('role');
            if(role == 'administrator'){
                const clientData = await db.collection(`client`).orderBy('createdAt', 'desc').get();
                // return an empty response if no result is found
                if (!clientData) {
                    return response.status(200).json(clients);
                }
                const cd = clientData.docs;
                if (cd && cd.length > 0) {
                    for (const c of cd) {
                            const myListaDetails = await db.collection(`lista`).orderBy('createdAt', 'desc').get();
                            const myListaSnaps = myListaDetails.docs;
                            let status;
                            if (myListaSnaps && myListaSnaps.length > 0) {
                                for (const myListaSnap of myListaSnaps) {
                                    if(myListaSnap.get('clientId') === c.id){
                                        if(myListaSnap.get('status') == 'Overdue'){
                                            status = 'Overdue'
                                            clients.push({
                                                id: c.id,
                                                clientId: c.get('clientId'),
                                                firstName: c.get('firstName'),
                                                lastName: c.get('lastName'),
                                                address: c.get('address'),
                                                gender: c.get('gender'),
                                                phoneNumber: c.get('phoneNumber'),
                                                status: status
                                            });
                                            break;
                                        }
                                    }
                                }
                            }
                    }
                }
            }else if(role == 'collector'){
                if (clientIDField) {
                for (const clientID of clientIDField) {
                    const clientData = await db.collection(`client`).orderBy('createdAt', 'desc').get();
                    // return an empty response if no result is found
                    if (!clientData) {
                        return response.status(200).json(clients);
                    }
                    const cd = clientData.docs;
                    if (cd && cd.length > 0) {
                        for (const c of cd) {
                            if(c.id === clientID){
                                const myListaDetails = await db.collection(`lista`).orderBy('createdAt', 'desc').get();
                                const myListaSnaps = myListaDetails.docs;
                                let status;
                                if (myListaSnaps && myListaSnaps.length > 0) {
                                    for (const myListaSnap of myListaSnaps) {
                                        if(myListaSnap.get('clientId') === clientID){
                                            if(myListaSnap.get('status') == 'Overdue'){
                                                status = 'Overdue'
                                                clients.push({
                                                    id: c.id,
                                                    clientId: c.get('clientId'),
                                                    firstName: c.get('firstName'),
                                                    lastName: c.get('lastName'),
                                                    address: c.get('address'),
                                                    gender: c.get('gender'),
                                                    phoneNumber: c.get('phoneNumber'),
                                                    status: status
                                                });
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            }
        }
    }
    return response.status(200).json(clients);
});

app.get('/dashboard/:id', async (request, response) => {
    const idParam = request.params['id'];
    const dashboard: any[] = [];
    const collectorDetails = await db.collection(`collector`).where(`uid`, `==`, `${idParam}`).get();
    // return an empty response if no result is found
    if (!collectorDetails || collectorDetails.docs.length === 0) {
        return response.status(200).json(dashboard);
    }
    const collectorSnaps = collectorDetails.docs;
    if (collectorSnaps && collectorSnaps.length > 0) {
        for (const cs of collectorSnaps) {
            const clientIDField: string[] = cs.get('clientId');
            const role: string = cs.get('role');
            if(role == 'administrator'){
                let totalClient = 0;
                let onTrackClient = 0;
                let onOverdueClient = 0;
                const clientData = await db.collection(`client`).orderBy('createdAt', 'desc').get();
                // return an empty response if no result is found
                if (!clientData) {
                    return response.status(200).json(dashboard);
                }
                const cd = clientData.docs;
                if (cd && cd.length > 0) {
                    for (const c of cd) {
                        const myListaDetails = await db.collection(`lista`).orderBy('createdAt', 'desc').get();
                        const myListaSnaps = myListaDetails.docs;
                        if (myListaSnaps && myListaSnaps.length > 0) {
                            for (const myListaSnap of myListaSnaps) {
                                if(myListaSnap.get('clientId') === c.id){
                                    if(myListaSnap.get('status') == 'On Track'){
                                        onTrackClient ++;
                                        totalClient ++;
                                        break;
                                    }else if(myListaSnap.get('status') == 'Overdue'){
                                        onOverdueClient ++;
                                        totalClient ++;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                dashboard.push({
                    totalClient: totalClient,
                    onTrackClient: onTrackClient,
                    onOverdueClient: onOverdueClient,
                });
            }else if(role == 'collector'){
                if (clientIDField) {
                    let totalClient = 0;
                    let onTrackClient = 0;
                    let onOverdueClient = 0;
                    for (const clientID of clientIDField) {
                        const clientData = await db.collection(`client`).orderBy('createdAt', 'desc').get();
                        // return an empty response if no result is found
                        if (!clientData) {
                            return response.status(200).json(dashboard);
                        }
                        const cd = clientData.docs;
                        if (cd && cd.length > 0) {
                            for (const c of cd) {
                                if(c.id === clientID){
                                    const myListaDetails = await db.collection(`lista`).orderBy('createdAt', 'desc').get();
                                    const myListaSnaps = myListaDetails.docs;
                                    if (myListaSnaps && myListaSnaps.length > 0) {
                                        for (const myListaSnap of myListaSnaps) {
                                            if(myListaSnap.get('clientId') === clientID){
                                                if(myListaSnap.get('status') == 'On Track'){
                                                    onTrackClient ++;
                                                    totalClient ++;
                                                    break;
                                                }else if(myListaSnap.get('status') == 'Overdue'){
                                                    onOverdueClient ++;
                                                    totalClient ++;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    dashboard.push({
                        totalClient: totalClient,
                        onTrackClient: onTrackClient,
                        onOverdueClient: onOverdueClient,
                    });
                }
            }
        }
    }
    return response.status(200).json(dashboard);
});

app.get('/clientAssign/:id', async (request, response) => {
    const idParam = request.params['id'];
    const clients: any[] = [];
    const clientDetails = await db.collection(`client`).get();
    // return an empty response if no result is found
    if (!clientDetails || clientDetails.docs.length === 0) {
        return response.status(200).json(clients);
    }
    const clientSnaps = clientDetails.docs;
    if (clientSnaps && clientSnaps.length > 0) {
        for (const cs of clientSnaps) {
            if(cs.get('collectorId') == idParam){
                clients.push({
                    id: cs.id,
                    clientId: cs.get('clientId'),
                    collectorId: cs.get('collectorId'),
                    firstName: cs.get('firstName'),
                    lastName: cs.get('lastName'),
                    address: cs.get('address'),
                    is_assigned: true,
                    status: cs.get('status')
                });
            }else if(cs.get('collectorId') == ''){
                clients.push({
                    id: cs.id,
                    clientId: cs.get('clientId'),
                    collectorId: cs.get('collectorId'),
                    firstName: cs.get('firstName'),
                    lastName: cs.get('lastName'),
                    address: cs.get('address'),
                    is_assigned: false,
                    status: cs.get('status')
                });
            }
        }
    }
    return response.status(200).json(clients);
});

app.get('/clientNo', async (request, response) => {
    const clientno = await db.collection(`clientNo`).get();
    const clientNo: any[] = [];
    // return an empty response if no result is found
    if (!clientno) {
      return response.status(200).json(clientNo);
    }
    const clientNoSnaps = clientno.docs;
    if (clientNoSnaps && clientNoSnaps.length > 0) {
        for (const clientSnap of clientNoSnaps) {
            clientNo.push({
                id: clientSnap.id,
                clientNo: clientSnap.get('clientNo')
            });
        }
    }
    return response.status(200).json(clientNo);
});

app.get('/client/:id', async (request, response) => {
    const idParam = request.params['id'];
    const clientDetail = await db.doc(`client/${idParam}`).get();
    
    const client: any[] = [];
    // return an empty response if no result is found
    if (!clientDetail) {
      return response.status(200).json(client);
    }
    if (clientDetail) {
        const myListaDetails = await db.collection(`lista`).orderBy('createdAt', 'desc').get();
        const myListaSnaps = myListaDetails.docs;
        let status = 'inactive';
        if (myListaSnaps && myListaSnaps.length > 0) {
            for (const myListaSnap of myListaSnaps) {
                if(myListaSnap.get('clientId') === idParam){
                    if(myListaSnap.get('status') !== 'Completed'){
                        status = 'active'
                        break;
                    }
                }
            }
        }
        client.push({
            id: clientDetail.id,
            clientId: clientDetail.get('clientId'),
            lista: clientDetail.get('lista'),
            firstName: clientDetail.get('firstName'),
            lastName: clientDetail.get('lastName'),
            address: clientDetail.get('address'),
            gender: clientDetail.get('gender'),
            phoneNumber: clientDetail.get('phoneNumber'),
            status: status
        });
    }
    return response.status(200).json(client);
});

app.get('/collector', async (request, response) => {
    const collectorDetails = await db.collection(`collector`).orderBy('createdAt', 'desc').get();
    
    const collector: any[] = [];
    // return an empty response if no result is found
    if (!collectorDetails || collectorDetails.docs.length === 0) {
        return response.status(200).json(collector);
    }

    const collectorSnaps = collectorDetails.docs;
    if (collectorSnaps && collectorSnaps.length > 0) {
        for (const collectorSnap of collectorSnaps) {
            collector.push({
                id: collectorSnap.id,
                firstName: collectorSnap.get('firstName'),
                lastName: collectorSnap.get('lastName'),
                address: collectorSnap.get('address'),
                gender: collectorSnap.get('gender'),
                phoneNumber: collectorSnap.get('phoneNumber'),
                uuid: collectorSnap.get('uid'),
                status: collectorSnap.get('status')
            });
        }
    }   
    
    return response.status(200).json(collector);
});

app.get('/mylista/:id', async (request, response) => {
    const idParam = request.params['id'];
    const listaDetails= await db.collection(`lista`).orderBy('createdAt', 'desc').get();
    const listas: any[] = [];
     // return an empty response if no result is found
    if (!listaDetails || listaDetails.docs.length === 0) {
        return response.status(200).json(listas);
    }
    const listaSnaps = listaDetails.docs;
    if (listaSnaps && listaSnaps.length > 0) {
        for (const listaSnap of listaSnaps) {
            if(listaSnap.get('clientId') === idParam){
                listas.push({
                    id: listaSnap.id,
                    listaId: listaSnap.get('listaId'),
                    clientId: listaSnap.get('clientId'),
                    description: listaSnap.get('description'),
                    alias: listaSnap.get('alias'),
                    status: listaSnap.get('status'),
                    totalAmount: listaSnap.get('totalAmount'),
                    startDate: listaSnap.get('startDate'),
                    endDate: listaSnap.get('endDate'),
                    dpa: listaSnap.get('dpa'),
                    amountPaid: listaSnap.get('amountPaid'),
                    balance: listaSnap.get('balance'),
                    createdAt: listaSnap.get('createdAt'),
                    updatedAt: listaSnap.get('updatedAt')
                });
            }
        }
    }
    return response.status(200).json(listas);
});

app.get('/mycollection/:id', async (request, response) => {
    const idParam = request.params['id'];
    const collectionDetails= await db.collection(`collection`).orderBy('createdAt', 'desc').get();
    const collections: any[] = [];
     // return an empty response if no result is found
    if (!collectionDetails || collectionDetails.docs.length === 0) {
        return response.status(200).json(collections);
    }
    const collectionSnaps = collectionDetails.docs;
    if (collectionSnaps && collectionSnaps.length > 0) {
        for (const collectionSnap of collectionSnaps) {
            if(collectionSnap.get('listaId') === idParam){
                collections.push({
                    id: collectionSnap.id,
                    amount: collectionSnap.get('amount'),
                    listaId: collectionSnap.get('listaId'),
                    createdAt: collectionSnap.get('createdAt'),
                    updatedAt: collectionSnap.get('updatedAt')
                });
            }
        }
    }
    return response.status(200).json(collections);
});

app.get('/startCollections/:id', async (request, response) => {
    const idParam = request.params['id'];
    let collections: any[] = [];
    let client: any[] = [];
    const collectorDetails = await db.collection(`collector`).where(`uid`, `==`, `${idParam}`).get();
    // return an empty response if no result is found
    if (!collectorDetails || collectorDetails.docs.length === 0) {
        return response.status(200).json(collections);
    }
    const collectorSnaps = collectorDetails.docs;
    if (collectorSnaps && collectorSnaps.length > 0) {
        for (const cs of collectorSnaps) {
            const clientIDField: string[] = cs.get('clientId');
            if (clientIDField) {
                for (const clientID of clientIDField) {
                    const clientData = await db.doc(`client/${clientID}`).get();
                    if (clientData) {
                        client.push({
                            clientId: clientData.id,
                            firstName: clientData.get('firstName'),
                            lastName: clientData.get('lastName'),
                        }) 
                    }
                }
                for (const c of client) {
                    const listaDetails = await db.collection(`lista`)
                    .where(`clientId`, `==`, `${c.clientId}`)
                    .where(`status`, `in`, ['On Track', 'Overdue'])
                    .orderBy(`createdAt`, `desc`)
                    .get();
                    const listaSnaps = listaDetails.docs;
                    if (listaSnaps && listaSnaps.length > 0) {
                        for (const listaSnap of listaSnaps) {
                            collections.push({
                                id: listaSnap.id,
                                clientId: c.clientId,
                                dpa: listaSnap.get('dpa'),
                                balance: listaSnap.get('balance'),
                                firstName: c.firstName,
                                lastName: c.lastName,
                                listaId: listaSnap.get('listaId'),
                                amountPaid: listaSnap.get('amountPaid'),
                                alias: listaSnap.get('alias'),
                            })
                        } 
                    }
                }
            }
        }
    }
    return response.status(200).json(collections);
});

app.get('/ranking/:id', async (request, response) => {
    const idParam = request.params['id'];
    let client: any[] = [];
    let rank: any = 1;
    const collectorDetails = await db.collection(`collector`).where(`uid`, `==`, `${idParam}`).get();
    // return an empty response if no result is found
    if (!collectorDetails || collectorDetails.docs.length === 0) {
        return response.status(200).json(client);
    }
    const collectorSnaps = collectorDetails.docs;
    if (collectorSnaps && collectorSnaps.length > 0) {
        for (const cs of collectorSnaps) {
            const clientIDField: string[] = cs.get('clientId');
            if (clientIDField) {
                for (const clientID of clientIDField) {
                    const clientData = await db.doc(`client/${clientID}`).get();
                    if (clientData) {
                        client.push({
                            collectorId: cs.id,
                            clientId: clientData.id,
                            rank: rank,
                            firstName: clientData.get('firstName'),
                            lastName: clientData.get('lastName'),
                        });
                        rank++;
                    }
                }
            }
        }
    }
    return response.status(200).json(client);
});

app.get('/collector/:id', async (request, response) => {
    const idParam = request.params['id'];
    let collector: any[] = [];
    const collectorDetails = await db.collection(`collector`).where(`uid`, `==`, `${idParam}`).get();
    // return an empty response if no result is found
    if (!collectorDetails || collectorDetails.docs.length === 0) {
        return response.status(200).json(collector);
    }
    const collectorSnaps = collectorDetails.docs;
    if (collectorSnaps && collectorSnaps.length > 0) {
        for (const cs of collectorSnaps) {
            collector.push({
                collectorId: cs.id,
                firstName: cs.get('firstName'),
                lastName: cs.get('lastName'),
                address: cs.get('address'),
                gender: cs.get('gender'),
                phoneNumber: cs.get('phoneNumber'),
                status: cs.get('status')
            });
        }
    }
    return response.status(200).json(collector);
});

export const youraccount = functions.https.onRequest(app);